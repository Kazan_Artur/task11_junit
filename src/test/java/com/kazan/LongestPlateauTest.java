package com.kazan;

import com.kazan.model.LongestPlateau;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongestPlateauTest {

    static int[] array;

    @BeforeAll
    public static void initArray() {
        array = new int[]{1, 2, 2, 2, 6, 5};
        System.out.println("Array is initialized");
    }

    @AfterAll
    public static void testBeforeAll() {
        System.out.println("all tests are passed");
    }

    @Test
    public void testGetLength() {
        LongestPlateau plateau = new LongestPlateau();
        assertEquals(plateau.getLongestPlate(array), 3);
        System.out.println("getLength() is correct!");
    }

    @Test
    public void testGetLocation() {
        LongestPlateau plateau = new LongestPlateau();
        assertEquals(plateau.getLocation(array), 3);
        System.out.println("getLocation() is correct!");
    }
}
