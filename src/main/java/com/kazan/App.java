package com.kazan;

import com.kazan.view.PlateauView;

public class App {

    public static void main(String[] args) {
        new PlateauView().start();
    }
}
