package com.kazan.view;

@FunctionalInterface
public interface Printable {

    void print();
}
